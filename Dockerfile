# this is file to create Docker tomcat auto start 

FROM pbeniwal/hellowebtomcat:v1

MAINTAINER Prashant Beniwal

COPY tomcat-users.xml /usr/local/tomcat/conf/
COPY context.xml /usr/local/tomcat/webapps/manager/META-INF/
COPY HelloWeb.war /usr/local/tomcat/webapps/
